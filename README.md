# quarkus3 project

### 1. Requirements
- Java 11
- Maven 3.6.3
 
### 2. Instructions to run
- compile the project
> mvn clean package
- run it!
> java -Dquarkus.http.port=8080 -jar quarkus3-runner.jar

### 3. How to get stuff
- open your browser and navigate to get stuff :)
> http://localhost:8080/stuff
