package com.lottoland.quarkus;

import io.quarkus.test.junit.NativeImageTest;

@NativeImageTest
public class NativeStuffResourceIT extends StuffResourceTest {

    // Execute the same tests but in native mode.
}